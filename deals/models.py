from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Deal(models.Model):
    title= models.CharField(max_length=120)
    slug= models.SlugField()
    description= models.CharField(max_length=120, null=True, blank=True)
    publish_date= models.DateTimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    expire_date= models.DateTimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    remaining = models.IntegerField(null=True, blank=True)
    price= models.DecimalField(decimal_places=2, max_digits=300, null=True, blank=True)
    deal_price= models.DecimalField(decimal_places=2, max_digits=300,null=True, blank=True)
    featured = models.BooleanField(default=False)
    active = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    updated = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    
    def __unicode__(self):
        return self.title
        
    
    
    