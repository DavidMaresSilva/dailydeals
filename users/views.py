# from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
# from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from .forms import UserRegisterForm

# Create your views here.

def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username=form.cleaned_data.get('username')
            messages.success(request, 'Cuenta Creada para {username}!')
            return redirect('home')
    else:
        form= UserRegisterForm()
    return render(request, 'users/register.html', {'form':form})
# return render(request, 'registration/registration_form.html', {'form':form})


# message.debug
