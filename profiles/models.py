from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in

import stripe
from dailydeals.stripe_info import secret_key
stripe.api_key = secret_key


class UserStripe(models.Model):
    user = models.OneToOneField(User)
    stripe_id = models.CharField(max_length=120, null=True, blank=True)
    phone_number = models.CharField(max_length=120,default='55-5555-5555')
    
    def __unicode__(self):
        return self.user.username

# Create your models here.

def CreateStripeId(sender,user,request, **kwargs):
    print "user logged in"
    
    
    user_logged_in.connect(CreateStripeId)









