from django.shortcuts import render_to_response, RequestContext, HttpResponseRedirect



def home(request):
    
    context=locals()
    template= "home.html"
    return render_to_response(template, context, context_instance=RequestContext(request))
